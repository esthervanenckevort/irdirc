package nl.allthingsdigital.irdirc.registry;

import nl.allthingsdigital.irdirc.registry.RegistryApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RegistryApplication.class)
@WebAppConfiguration
public class RegistryApplicationTests {

	@Test
	public void contextLoads() {
	}

}
