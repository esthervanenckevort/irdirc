/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.registry;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import nl.allthingsdigital.irdirc.Project;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames= { "participant_id", "project_id" }) )
public class Inclusion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private UUID puid;

    @ManyToOne
    private Participant participant;

    @ManyToOne
    private Project project;

    public Inclusion() {
    }

    public Inclusion(UUID puid, Participant participant, Project project) {
        this.puid = puid;
        this.participant = participant;
        this.project = project;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inclusion other = (Inclusion) obj;
        if (!Objects.equals(this.puid, other.puid)) {
            return false;
        }
        return true;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public UUID getPuid() {
        return puid;
    }

    public void setPuid(UUID puid) {
        this.puid = puid;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.puid);
        return hash;
    }
}
