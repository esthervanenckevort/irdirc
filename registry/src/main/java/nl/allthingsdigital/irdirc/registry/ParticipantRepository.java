/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.registry;

import nl.allthingsdigital.irdirc.registry.Participant;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
public interface ParticipantRepository extends CrudRepository<Participant, Long> {

}
