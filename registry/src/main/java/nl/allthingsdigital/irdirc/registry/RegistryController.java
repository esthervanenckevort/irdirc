/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.registry;

import java.util.UUID;
import nl.allthingsdigital.irdirc.registry.Participant;
import javax.inject.Inject;
import nl.allthingsdigital.irdirc.Project;
import nl.allthingsdigital.irdirc.registry.Inclusion;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@RestController
public class RegistryController {

    @Inject
    private ParticipantRepository participantRepository;

    @Inject
    private ProjectRepository projectRepository;

    @Inject
    private InclusionRepository inclusionRepository;

    @Inject
    private RegistryService registryService;

    @RequestMapping(value = "/participant/{participant}", method = RequestMethod.GET)
    public Participant getParticipant(@PathVariable("participant") final long id) {
        Participant participant = participantRepository.findOne(id);
        return participant != null ? participant : new Participant();
    }

    @RequestMapping(value = "/participant/register", method = RequestMethod.POST)
    public Participant registerParticipantSubmit(@RequestBody final Participant participant) {

        return participantRepository.save(participant);
    }

    @RequestMapping(value = "/project/{project}/addparticipant", method = RequestMethod.POST)
    public void addParticipantToProject(
            @RequestBody final Participant participant,
            @PathVariable("project") final long id) {

        Project project = projectRepository.findOne(id);
        registryService.registerParticipant(participant, project);
    }

    @RequestMapping(value = "/project/{project}/addpuid", method = RequestMethod.POST)
    public void addPUIDToProject(
            @RequestBody final UUID uuid,
            @PathVariable("project") final long id) {

        Project project = projectRepository.findOne(id);
        Inclusion inclusion = inclusionRepository.findOne(uuid);
        registryService.registerParticipant(inclusion.getParticipant(), project);

    }
}
