/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.registry;

import nl.allthingsdigital.irdirc.registry.Participant;
import java.util.UUID;
import javax.inject.Inject;
import nl.allthingsdigital.irdirc.Project;
import nl.allthingsdigital.irdirc.registry.Inclusion;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Service
public class RegistryService {

    @Inject
    private InclusionRepository inclusionRepository;

    @Async
    public void registerParticipant(final Participant participant, final Project project) {
        final int hash = calculateHash(participant);
        final UUID uuid = getPUID(hash, project);

        Inclusion inclusion = new Inclusion(uuid, participant, project);

        inclusionRepository.save(inclusion);
    }

    private int calculateHash(final Participant participant) {
        return participant.hashCode();
    }

    private UUID getPUID(final int hash, final Project project) {
        // TODO implement getting this from the project service
        return new UUID(hash, project.hashCode());
    }
}
