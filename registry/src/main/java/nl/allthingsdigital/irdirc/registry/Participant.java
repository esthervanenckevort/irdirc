/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.registry;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Entity
public class Participant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    /**
     * First family name of the person.
     */
    private String familyName;

    /**
     * First given name of the person.
     */
    private String firstName;

    /**
     * Date of birth, without timezone information.
     */
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    /**
     * Georef location of birth.
     */
    private String placeOfBirth;

//    /**
//     * The projects that this person participates in.
//     */
//    @OneToMany
//    private Set<Project> projects;
//
//    @ElementCollection
//    private Map<Project, UUID> studyIds;

//    public Participant() {
//        projects = new HashSet<>();
//        studyIds = new HashMap<>();
//    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participant other = (Participant) obj;
        if (!Objects.equals(this.familyName, other.familyName)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.placeOfBirth, other.placeOfBirth)) {
            return false;
        }
        return Objects.equals(this.dateOfBirth, other.dateOfBirth);
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

//    public Set<Project> getProjects() {
//        return projects;
//    }
//
//    public void setProjects(Set<Project> projects) {
//        this.projects = projects;
//    }
//
//    public Map<Project, UUID> getStudyIds() {
//        return studyIds;
//    }
//
//    public void setStudyIds(Map<Project, UUID> studyIds) {
//        this.studyIds = studyIds;
//    }
//
//    public void addProject(final Project project) {
//        projects.add(project);
//    }
//
//    public void removeProject(final Project project) {
//        projects.remove(project);
//        studyIds.remove(project);
//    }
//
//    public void addStudyId(final Project project, final UUID id) {
//        projects.add(project);
//        studyIds.put(project, id);
//    }
//
//    public void removeStudyId(final Project project, final UUID id) {
//        studyIds.remove(project, id);
//    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.familyName);
        hash = 73 * hash + Objects.hashCode(this.firstName);
        hash = 73 * hash + Objects.hashCode(this.dateOfBirth);
        hash = 73 * hash + Objects.hashCode(this.placeOfBirth);
        return hash;
    }

}
