/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.project;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;
import javax.inject.Inject;
import nl.allthingsdigital.irdirc.Project;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@RestController
public class ProjectController {

    @Inject
    private ProjectRepository projectRepository;

    @Inject
    private SecretRepository secretRepository;

    @RequestMapping(value = "/project/register", method = RequestMethod.POST)
    public Project registerProject(@RequestBody final Project project) {
        Random random = new Random();
        Project savedProject = projectRepository.save(project);;
        Secret secret = new Secret(random.nextLong(), project);
        secretRepository.save(secret);
        return savedProject;
    }

    @RequestMapping(value = "/project/{project}/rehashpii", method = RequestMethod.POST)
    public UUID rehashPII(
            @RequestBody final int hash,
            @PathVariable("project") final long id) throws NoSuchAlgorithmException {

        Project project = projectRepository.findOne(id);
        Secret secret = secretRepository.findByProject(project);

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(Longs.toByteArray(secret.getSecret()));
        md.update(Ints.toByteArray(hash));
        byte[] digest = md.digest();
        ByteBuffer buffer = ByteBuffer.allocate(digest.length);
        buffer.put(digest);

        // Todo get from mapping service
        return new UUID(buffer.getLong(), buffer.getLong());
    }
}
