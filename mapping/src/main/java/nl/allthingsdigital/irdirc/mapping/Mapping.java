/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.mapping;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@Entity
public class Mapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 16)
    private byte[] hashedPII;

    private UUID puid;

    public Mapping(byte[] hashedPII) {
        this.hashedPII = hashedPII;
    }

    public Mapping() {
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mapping other = (Mapping) obj;
        return Arrays.equals(this.hashedPII, other.hashedPII);
    }

    public byte[] getHashedPII() {
        return hashedPII;
    }

    public void setHashedPII(byte[] hashedPII) {
        this.hashedPII = hashedPII;
    }

    public UUID getPuid() {
        return puid;
    }

    public void setPuid(UUID puid) {
        this.puid = puid;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Arrays.hashCode(this.hashedPII);
        return hash;
    }

}
