/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.irdirc.mapping;

import java.util.UUID;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author <a href="mailto:david@allthingsdigital.nl">David van Enckevort</a>
 */
@RestController
public class MappingController {

    @Inject
    private MappingRepository mappingRepository;

    @RequestMapping(value = "/mapping/", method = RequestMethod.POST)
    public UUID getPUID(@RequestBody final byte[] hashedPII) {
        Mapping mapping;
        mapping = mappingRepository.findOne(hashedPII);
        if (mapping == null) {
            mapping = new Mapping(hashedPII);
            mapping.setPuid(UUID.randomUUID());
            mappingRepository.save(mapping);
        }
        return mapping.getPuid();
    }
}
